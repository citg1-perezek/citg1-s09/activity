package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController
//Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	ctrl + c to stop the server and application

//	localhost:8080/hello
	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}

//	Route with String Query
//	localhost:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam" annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name", defaultValue = "Khirt") String name){
		return String.format("Hi %s", name);
	}

//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Chuck") String name, @RequestParam(value="friend", defaultValue = "Joy") String friend){
		return  String.format("Hello %s! My name is %s.", friend, name);
	}

//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/name
	@GetMapping("/hello/{name}")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}

//	Activity for s09
ArrayList<String> enrollees = new ArrayList<String>();
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Joe") String user) {
		enrollees.add(user);
		return String.format("Thank you for Enrolling, %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String Enrollees() {
		return String.format(String.valueOf(enrollees));
	}

	@GetMapping("/nameage")
	//localhost:8080/greeting/nameage?name=value&age=value
	public String nameage(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s, My age is %d ", name, age);
	}

	@GetMapping("/courses/{id}")
	// "@PathVariable" allows us to extract data directly from the URL
	public String courses(@PathVariable("id") String id) {
		switch(id){
			case "java101":
				return String.format("Name: Java 101,Schedule: MWF 8:00AM-11:00AM, Price: PHP 3000.00");
			case "sql101":
				return String.format("Name: SQL 101, Schedule: TTH 1:00PM-04:00PM, Price: PHP 2000.00");
			case "javaee101":
				return String.format("Name: Java EE 101, Schedule: MFW 1:00PM-04:00PM, Price: PHP 3500.00");
			default:
				return String.format("Course cannot be found.");

		}
	}
}
